package com.jmsb.taiwan.library;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created with IntelliJ IDEA.
 * User: jmsb
 * Date: 8/15/13
 * Time: 2:48 PM
 * To change this template use File | Settings | File Templates.
 */
public class NetworkConnectionDetector {
    public static boolean isConnected(Context context){
        ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if(networkInfo !=null && networkInfo.isConnectedOrConnecting())
            return true;
        return false;
    }
}
