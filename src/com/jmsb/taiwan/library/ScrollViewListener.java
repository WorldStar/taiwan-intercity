package com.jmsb.taiwan.library;

public interface ScrollViewListener 
{
    void onScrollChanged(CustomizedScrollView scrollView, int x, int y, int oldx, int oldy);
}