package com.jmsb.taiwan.leaguetable;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.*;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.jmsb.taiwan.R;
import com.jmsb.taiwan.library.DBHelper;
import com.jmsb.taiwan.library.ReturnMessage;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: jmsb
 * Date: 8/2/13
 * Time: 12:22 PM
 * To change this template use File | Settings | File Templates.
 */
public class LeagueTable extends Activity {
    DBHelper db;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.leaguetable);
        db = new DBHelper(getContext());
        new LoadLeagueTable(getContext()).execute();
    }

    private Context getContext() {
        Context context;
        if (getParent() != null) context = getParent();
        else context = this;
        return context;
    }

    private class LoadLeagueTable extends AsyncTask<Void, Void, Void> {
        ProgressDialog progressDialog;
        Context context;

        private LoadLeagueTable(Context context){
            super();
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setTitle(getString(R.string.LoadingDialog_Title));
            progressDialog.setMessage(getString(R.string.LoadingDialog_Content1));
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            NodeList teamNodeList = getContentFromXML(getString(R.string.LeagueTableUrl), "team");
            if(teamNodeList != null){
                db.dropLeagueTable();
                List<LeagueTableObject> teamList = convertNodeToObjectList(teamNodeList);
            }
            return null;  //To change body of implemented methods use File | Settings | File Templates.
        }

        @Override
        protected void onPostExecute(Void v) {
            super.onPostExecute(v);
            progressDialog.dismiss();
            List<LeagueTableObject> list = db.getLeagueTable();
            if(list != null){
                showLeagueTable(context, list);
            }else{
                ReturnMessage.showAlertDialog(context, "Unable to load", "Unable to load League Table, please try again later.");
            }
        }

        private void showLeagueTable(Context context, List<LeagueTableObject> list) {
            ListView leaguetableListView = (ListView)findViewById(R.id.listview);
            LeagueTableAdapter leagueTableAdapter = new LeagueTableAdapter(context, 0, list);
            leaguetableListView.setAdapter(leagueTableAdapter);
        }
    }

    private class LeagueTableAdapter extends ArrayAdapter<LeagueTableObject>{
        Context context;
        List<LeagueTableObject> list;

        public class LeagueTableHolder{
            TextView position;
            TextView team;
            TextView played;
            TextView won;
            TextView draw;
            TextView lost;
            TextView goal;
            TextView diff;
            TextView pts;
            ImageView teamlogo;
        }

        public LeagueTableAdapter(Context context, int a, List<LeagueTableObject> list) {
            super(context, a, list);
            this.context = context;
            this.list = list;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LeagueTableHolder holder;
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            if(convertView == null){
                convertView = inflater.inflate(R.layout.leaguetable_row, parent, false);
                holder = new LeagueTableHolder();
                holder.position = (TextView)convertView.findViewById(R.id.leaguetable_position);
                holder.team = (TextView)convertView.findViewById(R.id.leaguetable_name);
                holder.played = (TextView)convertView.findViewById(R.id.leaguetable_played);
                holder.won = (TextView)convertView.findViewById(R.id.leaguetable_won);
                holder.draw = (TextView)convertView.findViewById(R.id.leaguetable_draw);
                holder.lost = (TextView)convertView.findViewById(R.id.leaguetable_lost);
                holder.goal = (TextView)convertView.findViewById(R.id.leaguetable_goal);
                holder.diff = (TextView)convertView.findViewById(R.id.leaguetable_diff);
                holder.pts = (TextView)convertView.findViewById(R.id.leaguetable_pts);
                holder.teamlogo = (ImageView)convertView.findViewById(R.id.ivLocalImage);
                convertView.setTag(holder);
            }
            else
                holder = (LeagueTableHolder)convertView.getTag();
            LeagueTableObject leagueTableObject = list.get(position);
            holder.position.setText(String.valueOf(leagueTableObject.getPosition()));
            holder.team.setText(leagueTableObject.getTeam_name());
            holder.played.setText(String.valueOf(leagueTableObject.getGame_played()));
            holder.won.setText(String.valueOf(leagueTableObject.getGame_won()));
            holder.draw.setText(String.valueOf(leagueTableObject.getGame_draw()));
            holder.lost.setText(String.valueOf(leagueTableObject.getGame_lost()));
            holder.goal.setText(leagueTableObject.getGoal_scored()+":"+leagueTableObject.getGoal_against());
            holder.diff.setText((String.valueOf(leagueTableObject.getGoal_scored()-leagueTableObject.getGoal_against())));
            holder.pts.setText(String.valueOf(leagueTableObject.getPoint()));
            File imgFile = new File(Environment.getExternalStorageDirectory().toString()+"/TaiwanIntercity/"+leagueTableObject.getTeamID()+".png");
            if(imgFile.exists()){
                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                holder.teamlogo.setImageBitmap(myBitmap);
            }else{
                holder.teamlogo.setImageResource(R.drawable.rihanna);
            }
            return convertView;
        }
    }

    private List<LeagueTableObject> convertNodeToObjectList(NodeList nodeList) {
        Node node = null;
        List<LeagueTableObject> teamList= new ArrayList<LeagueTableObject>();
        LeagueTableObject team;
        int nodeListSize = nodeList.getLength();
        for(int i=0; i<nodeListSize; i++){
            node = nodeList.item(i);
            team = new LeagueTableObject();
            team.setPosition(i+1);
            team.setTeam_name(((Element) node).getAttribute("name"));
            team.setGame_played(Integer.parseInt(((Element) node).getAttribute("gp")));
            team.setGame_won(Integer.parseInt(((Element) node).getAttribute("w")));
            team.setGame_draw(Integer.parseInt(((Element) node).getAttribute("d")));
            team.setGame_lost(Integer.parseInt(((Element)node).getAttribute("l")));
            team.setGoal_scored(Integer.parseInt(((Element)node).getAttribute("gs")));
            team.setGoal_against(Integer.parseInt(((Element)node).getAttribute("ga")));
            team.setPoint(Integer.parseInt(((Element)node).getAttribute("p")));
            team.setTeamID(((Element)node).getAttribute("id"));
            teamList.add(team);
            db.setLeagueTable(team);
        }
        return teamList;
    }

    private NodeList getContentFromXML(String urlString, String tagName) {
        NodeList nodeList = null;
        try {
            URL url = new URL(urlString);
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(new InputSource(url.openStream()));
            doc.getDocumentElement().normalize();
            nodeList = doc.getElementsByTagName(tagName);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return nodeList;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.option_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_refresh:
                new LoadLeagueTable(getContext()).execute();
                break;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage(getString(R.string.ExitDialog_Content)).setCancelable(false);
        builder.setNegativeButton(getString(R.string.ExitDialog_NoButton), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.setPositiveButton(getString(R.string.ExitDialog_YesButton), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }
}