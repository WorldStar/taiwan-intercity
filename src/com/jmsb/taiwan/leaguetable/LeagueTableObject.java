package com.jmsb.taiwan.leaguetable;

/**
 * Created with IntelliJ IDEA.
 * User: jmsb
 * Date: 8/2/13
 * Time: 3:46 PM
 * To change this template use File | Settings | File Templates.
 */
public class LeagueTableObject {
    String team_name;
    int game_played;
    int game_won;
    int game_draw;
    int game_lost;
    int goal_scored;
    int goal_against;
    int goal_different;
    int point;
    int position;
    String teamID;

    public String getTeamID() {
        return teamID;
    }

    public void setTeamID(String teamID) {
        this.teamID = teamID;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getTeam_name() {
        return team_name;
    }

    public void setTeam_name(String team_name) {
        this.team_name = team_name;
    }

    public int getGame_played() {
        return game_played;
    }

    public void setGame_played(int game_played) {
        this.game_played = game_played;
    }

    public int getGame_won() {
        return game_won;
    }

    public void setGame_won(int game_won) {
        this.game_won = game_won;
    }

    public int getGame_draw() {
        return game_draw;
    }

    public void setGame_draw(int game_draw) {
        this.game_draw = game_draw;
    }

    public int getGame_lost() {
        return game_lost;
    }

    public void setGame_lost(int game_lost) {
        this.game_lost = game_lost;
    }

    public int getGoal_scored() {
        return goal_scored;
    }

    public void setGoal_scored(int goal_scored) {
        this.goal_scored = goal_scored;
    }

    public int getGoal_against() {
        return goal_against;
    }

    public void setGoal_against(int goal_against) {
        this.goal_against = goal_against;
    }

    public int getGoal_different() {
        return goal_different;
    }

    public void setGoal_different(int goal_different) {
        this.goal_different = goal_different;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }
}
