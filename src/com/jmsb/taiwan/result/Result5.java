package com.jmsb.taiwan.result;

import java.util.List;
import java.util.Vector;

import com.jmsb.taiwan.R;

import android.app.Activity;
import android.content.Context;
import android.net.ParseException;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;




public class Result5 extends Activity{
	private LayoutInflater mInflater;
	private Vector<RowData> data;
	RowData rd;
	
	TextView tvLocalTeam, tvVisitorTeam, tvLocalScore, tvVisitorScore;
	ImageView ivLocalImage, ivVisitImage;
	TextView tvLocalPlayer, tvTime, tvVisitPlayer;
	
	static final String[] timeP = new String[] {
		"44'", "59'", "90'"};
	static final String[] localP = new String[] {
		"Victor", "Victor", null};
	static final String[] visitP = new String[] {
		"", "", "Higuain - Benzema"};
	
	private Integer[] localImageP = {R.drawable.yellowcard, R.drawable.redcard, R.drawable.white};
	private Integer[] visitImageP = {R.drawable.white, R.drawable.white, R.drawable.swap};
	
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.result);
		
		tvLocalTeam = (TextView)findViewById(R.id.tvLocalTeam);
		tvVisitorTeam = (TextView)findViewById(R.id.tvVisitorTeam);
		tvLocalScore = (TextView)findViewById(R.id.tvLocalScore);
		tvVisitorScore = (TextView)findViewById(R.id.tvVisitorScore);
		
		tvLocalTeam.setText("Mallorca");
		tvVisitorTeam.setText("Real Madrid");
		tvLocalScore.setText("0");
		tvVisitorScore.setText("0");
		
		ListView lvScore = (ListView) findViewById(R.id.listview1);
		
		mInflater = (LayoutInflater) getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		data = new Vector<RowData>();
		
		for (int i = 0; i < timeP.length; i++) {
			try {
				rd = new RowData(i, timeP[i]);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			data.add(rd);
		}
		
		CustomAdapter adapter = new CustomAdapter(this, R.layout.resultlist, R.id.title, data);
		lvScore.setAdapter(adapter);
		lvScore.setTextFilterEnabled(true);

	}
	
	private class RowData {
		protected int mId;
		protected String mTitle;

		RowData(int id, String title) {
			mId = id;
			mTitle = title;
		}

		@Override
		public String toString() {
			return mId + " " + mTitle;
		}
	}
	
	private class CustomAdapter extends ArrayAdapter<RowData> {
		public CustomAdapter(Context context, int resource,
				int textViewResourceId, List<RowData> objects) {
			super(context, resource, textViewResourceId, objects);		
		}
		
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder = null;
			TextView localPlayer = null;
			TextView visitPlayer = null;
			TextView time = null;
			ImageView localImage = null;
			ImageView visitImage = null;
			
			RowData rowData = getItem(position);
			if (null == convertView) {
				convertView = mInflater.inflate(R.layout.resultlist, null);
				holder = new ViewHolder(convertView);
				convertView.setTag(holder);
			}
			holder = (ViewHolder) convertView.getTag();

			time = holder.getTime();
			time.setText(rowData.mTitle);

			localPlayer = holder.getLocalPlayer();
			localPlayer.setText(localP[rowData.mId]);

			visitPlayer = holder.getVisitPlayer();
			visitPlayer.setText(visitP[rowData.mId]);
			
			localImage = holder.getLocalImage();
			localImage.setImageResource(localImageP[rowData.mId]);
			
			visitImage = holder.getVisitImage();
			visitImage.setImageResource(visitImageP[rowData.mId]);
			
			return convertView;
		}

		private class ViewHolder {
			private View mRow;
			TextView localPlayer = null;
			TextView visitPlayer = null;
			TextView time = null;
			ImageView localImage = null;
			ImageView visitImage = null;

			public ViewHolder(View row) {
				mRow = row;
			}

			public TextView getLocalPlayer() {
				if (null == localPlayer) {
					localPlayer = (TextView) mRow
							.findViewById(R.id.tvLocalPlayer);
				}
				return localPlayer;
			}

			public TextView getVisitPlayer() {
				if (visitPlayer == null) {
					visitPlayer = (TextView) mRow.findViewById(R.id.tvVisitPlayer);
				}
				return visitPlayer;
			}
			
			public TextView getTime() {
				if (time == null) {
					time = (TextView) mRow.findViewById(R.id.tvTime);
				}
				return time;
			}

			public ImageView getLocalImage() {
				if (null == localImage) {
					localImage = (ImageView) mRow.findViewById(R.id.ivLocalImage);
				}
				return localImage;
			}
			
			public ImageView getVisitImage() {
				if (null == visitImage) {
					visitImage = (ImageView) mRow.findViewById(R.id.ivVisitImage);
				}
				return visitImage;
			}
		}
	}


}
