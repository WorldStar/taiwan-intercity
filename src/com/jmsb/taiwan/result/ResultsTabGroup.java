package com.jmsb.taiwan.result;

import android.content.Intent;
import android.os.Bundle;

import com.jmsb.taiwan.library.TabGroupActivity;

public class ResultsTabGroup extends TabGroupActivity{
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		startChildActivity("ResultsActivity", new Intent(this, Results.class));
	}
}