package com.jmsb.taiwan.video;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.*;
import android.widget.*;
import com.jmsb.taiwan.R;
import com.jmsb.taiwan.library.DBHelper;
import com.jmsb.taiwan.library.ReturnMessage;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: jmsb
 * Date: 8/16/13
 * Time: 9:51 AM
 * To change this template use File | Settings | File Templates.
 */
public class Video extends Activity {
    DisplayImageOptions options;
    protected ImageLoader imageLoader = ImageLoader.getInstance();
    DBHelper db;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listview);
        TextView header = (TextView)findViewById(R.id.header);
        header.setText(getString(R.string.HeaderTitle_Video));
        db = new DBHelper(getContext());
        //Universal image loader config
        options = new DisplayImageOptions.Builder()
                .showStubImage(R.drawable.ic_stub)
                .showImageForEmptyUri(R.drawable.ic_empty)
                .showImageOnFail(R.drawable.ic_error)
                .cacheInMemory()
                .cacheOnDisc()
                .displayer(new RoundedBitmapDisplayer(20))
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .defaultDisplayImageOptions(options)
                .build();
        ImageLoader.getInstance().init(config);

        new LoadVideo(getContext()).execute();
    }

    private Context getContext() {
        Context context;
        if (getParent() != null) context = getParent();
        else context = this;
        return context;
    }

    public class LoadVideo extends AsyncTask<Void, Void, List<VideoObject>> {
        Context context;
        ProgressDialog progressDialog;

        public LoadVideo(Context context){
            super();
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setTitle(getString(R.string.LoadingDialog_Title));
            progressDialog.setMessage(getString(R.string.LoadingDialog_Content1));
            progressDialog.show();
        }

        @Override
        protected List<VideoObject> doInBackground(Void... voids) {
            return getVideoList();
        }

        private List<VideoObject> getVideoList() {
            //if timestamp smaller than local timestamp, do this
            NodeList newsNodeList = getContentFromXML(getString(R.string.VideoUrl), "post");
            if(newsNodeList != null){
                db.dropVideoTable();
                List<VideoObject> videoObjectList = new ArrayList<VideoObject>();
                videoObjectList = convertNewsNodeToObjectList(newsNodeList);
                return videoObjectList;
            }else{
                return db.getVideo();
            }
        }

        private NodeList getContentFromXML(String urlString, String tagName) {
            NodeList nodeList = null;
            try {
                URL url = new URL(urlString);
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                Document doc = db.parse(new InputSource(url.openStream()));
                doc.getDocumentElement().normalize();
                nodeList = doc.getElementsByTagName(tagName);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            } catch (SAXException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return nodeList;
        }

        private List<VideoObject> convertNewsNodeToObjectList(NodeList nodeList) {
            Node node = null;
            List<VideoObject> videoList= new ArrayList<VideoObject>();
            VideoObject videoObject;
            int videoListSize = nodeList.getLength();
            for(int i=0; i<videoListSize; i++){
                node = nodeList.item(i);
                videoObject = new VideoObject();
                videoObject.setTitle(((Element) node).getAttribute("title"));
                videoObject.setContent(((Element) node).getAttribute("description"));
                videoObject.setVideoUrl(((Element) node).getAttribute("url"));
                String videourl[] = videoObject.getVideoUrl().split("=");
                videoObject.setThumbnailUrl("http://img.youtube.com/vi/"+videourl[1]+"/1.jpg");
                videoList.add(videoObject);
                db.setVideo(videoObject);
            }
            return videoList;
        }

        @Override
        protected void onPostExecute(List<VideoObject> list) {
            super.onPostExecute(list);
            progressDialog.dismiss();
            if(list!=null){
                showVideoList(context, list);
            }else{
                ReturnMessage.showAlertDialog(context, "Unable to load", "Unable to load video list, please try again later.");
            }
        }

        private void showVideoList(Context context, final List<VideoObject> list) {
            ListView videoListView = (ListView)findViewById(R.id.listview);
            VideoAdapter videoAdapter = new VideoAdapter(context,0,0, list);
            videoListView.setAdapter(videoAdapter);
            videoListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    VideoObject videoObject = list.get(i);
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(videoObject.getVideoUrl())));
                }
            });
        }

        private class VideoAdapter extends ArrayAdapter<VideoObject> {
            Context context;
            List<VideoObject> list;
            public VideoAdapter(Context context, int resource,
                               int textViewResourceId, List<VideoObject> list) {
                super(context, resource, textViewResourceId, list);
                this.context = context;
                this.list = list;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                NewsHolder holder = null;
                LayoutInflater inflater = ((Activity)context).getLayoutInflater();
                if(convertView == null){
                    convertView = inflater.inflate(R.layout.newslist, parent, false);
                    holder = new NewsHolder();
                    holder.headline = (TextView)convertView.findViewById(R.id.tvNewsHeadline);
                    holder.content = (TextView)convertView.findViewById(R.id.tvNewsContent);
                    holder.image = (ImageView)convertView.findViewById(R.id.ivNewsImage);
                    holder.button = (Button)convertView.findViewById(R.id.btReadMore);
                    convertView.setTag(holder);
                }
                else
                    holder = (NewsHolder)convertView.getTag();
                VideoObject videoObject = list.get(position);
                holder.headline.setText(videoObject.getTitle());
                holder.content.setMaxLines(5);
                holder.content.setText(videoObject.getContent());
                imageLoader.displayImage(videoObject.getThumbnailUrl(), holder.image);
                holder.button.setVisibility(View.GONE);
                return convertView;
            }

            private class NewsHolder {
                private TextView headline;
                private TextView content;
                private ImageView image;
                private Button button;
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.option_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_refresh:
                new LoadVideo(getContext()).execute();
                break;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage(getString(R.string.ExitDialog_Content)).setCancelable(false);
        builder.setNegativeButton(getString(R.string.ExitDialog_NoButton), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.setPositiveButton(getString(R.string.ExitDialog_YesButton), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }
}