package com.jmsb.taiwan.home;

import android.content.Intent;
import android.os.Bundle;

import com.jmsb.taiwan.library.TabGroupActivity;

public class HomeTabGroup extends TabGroupActivity{
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		startChildActivity("HomeActivity", new Intent(this, Home.class));
	}
}