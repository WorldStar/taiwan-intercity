package com.jmsb.taiwan.schedule;

/**
 * Created with IntelliJ IDEA.
 * User: jmsb
 * Date: 8/5/13
 * Time: 11:09 AM
 * To change this template use File | Settings | File Templates.
 */
public class ScheduleObject {
    String homeTeam;
    String awayTeam;
    String location;

    public int getMatchNumber() {
        return matchNumber;
    }

    public void setMatchNumber(int matchNumber) {
        this.matchNumber = matchNumber;
    }

    int matchNumber;

    public void setHomeTeam(String homeTeam) {
        this.homeTeam = homeTeam;
    }

    public void setAwayTeam(String awayTeam) {
        this.awayTeam = awayTeam;
    }

    public void setHomeScore(String homeScore) {
        this.homeScore = homeScore;
    }

    public void setAwayScore(String awayScore) {
        this.awayScore = awayScore;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setHomeTeamID(String homeTeamID) {
        this.homeTeamID = homeTeamID;
    }

    public void setAwayTeamID(String awayTeamID) {
        this.awayTeamID = awayTeamID;
    }

    String homeScore;
    String awayScore;
    String date;
    String time;
    String homeTeamID;
    String awayTeamID;
    int matchDay;

    public int getMatchDay() {
        return matchDay;
    }

    public void setMatchDay(int matchDay) {
        this.matchDay = matchDay;
    }

    public String getAwayTeamID() {
        return awayTeamID;
    }

    public String getHomeTeamID() {
        return homeTeamID;
    }

    public void setHomeAndAwayTeamID(String homeAndAwayTeamID) {
        String homeAwayTeamID[] = homeAndAwayTeamID.split("-");
        this.homeTeamID = homeAwayTeamID[0];
        this.awayTeamID = homeAwayTeamID[1];
    }

    public String getTime() {
        return time;
    }

    public String getDate() {
        return date;
    }

    public void setDateAndTime(String dateAndTime) {
        String dateTime[] = dateAndTime.split(" ");
        this.date = dateTime[0];
        this.time = dateTime[1];
    }

    public String getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeamAndAwayTeam(String homeTeamAndAwayTeam) {
        String homeAwayTeam[] = homeTeamAndAwayTeam.split("-");
        this.homeTeam = homeAwayTeam[0];
        this.awayTeam = homeAwayTeam[1];
    }

    public String getAwayTeam() {
        return awayTeam;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getHomeScore() {
        return homeScore;
    }

    public void setHomeScoreAndAwayScore(String homeScoreAndAwayScore) {
        String homeAwayScore[] = homeScoreAndAwayScore.split("-");
        if(homeAwayScore.length!=0){
            this.homeScore = homeAwayScore[0];
            this.awayScore = homeAwayScore[1];
        }else{
            this.homeScore = " ";
            this.awayScore = " ";
        }
    }

    public String getAwayScore() {
        return awayScore;
    }
}
