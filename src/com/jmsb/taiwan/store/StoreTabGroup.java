package com.jmsb.taiwan.store;

import android.content.Intent;
import android.os.Bundle;

import com.jmsb.taiwan.library.TabGroupActivity;

public class StoreTabGroup extends TabGroupActivity{
	
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		startChildActivity("StoreActivity", new Intent(this, Store.class));
	}

}
