package com.jmsb.taiwan.store;



import com.jmsb.taiwan.R;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class Store extends Activity{
	
	WebView webview;
	
	 public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.store);
	        
	        webview = new WebView(this);
	        webview.getSettings().setJavaScriptEnabled(true);
	        final Activity activity	= this;
	        
	        webview.setWebViewClient(new WebViewClient(){
	        	public void onReceivedError(WebView view, int errorCode, String description, String failingUrl){
	        		Toast.makeText(activity, description, Toast.LENGTH_SHORT).show();
	        	}
	        });
	        
	        webview.loadUrl("http://fantasy4all.com/store/index.php?route=common/all_categories");
	        setContentView(webview);
	        
	 }
	

}
