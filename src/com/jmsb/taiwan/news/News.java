package com.jmsb.taiwan.news;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.*;
import android.webkit.WebView;
import android.widget.*;
import com.jmsb.taiwan.R;
import com.jmsb.taiwan.library.DBHelper;
import com.jmsb.taiwan.library.ReturnMessage;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

public class News extends Activity {

    private LayoutInflater mInflater;
    private Vector<RowData> data;
    RowData rd;

	/*
	static final String[] newsHeadline = new String[] {
			"Messi leads inaugural AP football poll", 
			"La Liga Team of the Week: Messi & Ronaldo joined by Fabregas & Higuain",
			"United�s German transfer target happy in La Liga", 
			"�50m Tottenham star is Real Madrid�s top summer target",
			"Spanish La Liga Team of the Week" };

	static final String[] newsContent = new String[] {
			"Lionel Messi is the best footballer in the world...",
			"It was a routine weekend for Barcelona and Real...",
			"Manchester United transfer target Mesut Ozil...",
			"Tottenham star Gareth Bale is reportedly Real......",
			"Barcelona romped to a 5-0 win over a resilient Rayo..." };

	private Integer[] newsImage = { R.drawable.rmnewimage1,
			R.drawable.rmnewimage2, R.drawable.rmnewimage3,
			R.drawable.rmnewimage4, R.drawable.rmnewimage5 };
			*/

    String[] tvNewsHeadline;
    String[] tvContent;
    String[] subImage;
    String temp, temp1,testing;
    String[] tempCount;
    String[] webCont;
    String[] date;

    ProgressDialog progDialog;

    DisplayImageOptions options;
    BaseActivity base;
    protected ImageLoader imageLoader = ImageLoader.getInstance();
    View newsListLayout, newsDetailLayout;
    boolean isNewsDetailShow = false;
    TextView newsDetailTitle, newsDetailDate;
    WebView newsDetailContent;
    ImageView newsDetailImage;
    View scrollview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listview);
//		progDialog = ProgressDialog.show(getParent(), "Getting data", "Loading...");
        TextView header = (TextView)findViewById(R.id.header);
        header.setText(getString(R.string.HeaderTitle_News));
        newsListLayout = findViewById(R.id.mainlayout);
        newsDetailLayout = findViewById(R.id.newsdetail);
        newsDetailTitle = (TextView)findViewById(R.id.newsdetailheadline);
        newsDetailDate = (TextView)findViewById(R.id.newsdetailpublishdate);
        newsDetailContent = (WebView)findViewById(R.id.newsdetailwebview);
        newsDetailImage = (ImageView)findViewById(R.id.ivNewsImage);
        scrollview = findViewById(R.id.scrollView1);
        options = new DisplayImageOptions.Builder()
                .showStubImage(R.drawable.ic_stub)
                .showImageForEmptyUri(R.drawable.ic_empty)
                .showImageOnFail(R.drawable.ic_error)
                .cacheInMemory()
                .cacheOnDisc()
                .displayer(new RoundedBitmapDisplayer(20))
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .defaultDisplayImageOptions(options)
                .build();

        ImageLoader.getInstance().init(config);

        new LoadNews(getContext(),false).execute();
		/*try {

			URL url = new URL("http://fantasy4all.com/rmdemo/?feed=rss2");
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(new InputSource(url.openStream()));
			doc.getDocumentElement().normalize();

			NodeList nodeList = doc.getElementsByTagName("item");
			
			// Assign textview array lenght by arraylist size
			tvNewsHeadline = new String[nodeList.getLength()];
			tvContent = new String[nodeList.getLength()];
			tempCount = new String[nodeList.getLength()];
			subImage = new String[nodeList.getLength()];
			webCont = new String[nodeList.getLength()];
			date = new String[nodeList.getLength()];
			
			for (int i = 0; i < nodeList.getLength(); i++) {

				Node node = nodeList.item(i);
				
				Element fstElmnt = (Element) node;
				NodeList nameList = fstElmnt.getElementsByTagName("title");
				Element nameElement = (Element) nameList.item(0);
				nameList = nameElement.getChildNodes();
				tvNewsHeadline[i] = ((Node)nameList.item(0)).getNodeValue();
				
				NodeList websiteList = fstElmnt.getElementsByTagName("description");
				Element websiteElement = (Element) websiteList.item(0);
				websiteList = websiteElement.getChildNodes();
				tvContent[i] = ((Node) websiteList.item(0)).getNodeValue();
				
				NodeList webContent = fstElmnt.getElementsByTagName("content:encoded");
				Element webContentElement = (Element) webContent.item(0);
				webContent = webContentElement.getChildNodes();
				webCont[i] = ((Node) webContent.item(0)).getNodeValue();
				
				NodeList dateList = fstElmnt.getElementsByTagName("pubDate");
				Element dateElement = (Element) dateList.item(0);
				dateList = dateElement.getChildNodes();
				date[i] = ((Node) dateList.item(0)).getNodeValue();
				
				temp = tvContent[i];
				tempCount = temp.split("\"");			
				temp1 = tempCount[1];
				String noHTMLString = temp.replaceAll("\\<.*?>","");
				tvContent[i] = noHTMLString;
				subImage[i] = temp1;
			}
	
		} catch (Exception e) {
			System.out.println("XML Pasing Excpetion = " + e);
		}
		
		ListView lvNews = (ListView) findViewById(R.id.listview);

		mInflater = (LayoutInflater) getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		data = new Vector<RowData>();
		for (int i = 0; i < tvNewsHeadline.length; i++) {
			try {
				rd = new RowData(i, tvNewsHeadline[i]);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			data.add(rd);
		}
		CustomAdapter adapter = new CustomAdapter(this, R.layout.newslist,
				R.id.title, data);
		lvNews.setAdapter(adapter);
		lvNews.setTextFilterEnabled(true);
		
		//
		//if(tvNewsHeadline.length>1){
		//	progDialog.dismiss();
		//}

		lvNews.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int position,
					long id) {
				// TODO Auto-generated method stub
				
				Intent intent = new Intent(getParent(), NewsDetail1.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				Bundle b = new Bundle();
				b.putString("title", tvNewsHeadline[position]);
				b.putString("date", date[position]);
				b.putString("image", subImage[position]);
				b.putString("content", webCont[position]);
				intent.putExtras(b);
				startActivity(intent);
				
				/*
				switch (position) {
				case 0:
					Intent intent1 = new Intent(getParent(), NewsDetail1.class);
					intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					TabGroupActivity parentActivity1 = (TabGroupActivity) getParent();
					parentActivity1.startChildActivity("NewsDetail1", intent1);
					break;
				case 1:
					Intent intent2 = new Intent(getParent(), NewsDetail2.class);
					intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					TabGroupActivity parentActivity2 = (TabGroupActivity) getParent();
					parentActivity2.startChildActivity("NewsDetail2", intent2);
					break;
				case 2:
					Intent intent3 = new Intent(getParent(), NewsDetail3.class);
					intent3.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					TabGroupActivity parentActivity3 = (TabGroupActivity) getParent();
					parentActivity3.startChildActivity("NewsDetail3", intent3);
					break;
				case 3:
					Intent intent4 = new Intent(getParent(), NewsDetail4.class);
					intent4.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					TabGroupActivity parentActivity4 = (TabGroupActivity) getParent();
					parentActivity4.startChildActivity("NewsDetail4", intent4);
					break;
				case 4:
					Intent intent5 = new Intent(getParent(), NewsDetail5.class);
					intent5.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					TabGroupActivity parentActivity5 = (TabGroupActivity) getParent();
					parentActivity5.startChildActivity("NewsDetail5", intent5);
					break;
				}
			}
		});*/
    }

    private Context getContext() {
        Context context;
        if (getParent() != null) context = getParent();
        else context = this;
        return context;
    }

    public class LoadNews extends AsyncTask<Void, Void, List<NewsObject>>{
        Context context;
        ProgressDialog progressDialog;
        boolean isRefresh;

        public LoadNews(Context context, boolean isRefresh){
            super();
            this.context = context;
            this.isRefresh = isRefresh;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setTitle(getString(R.string.LoadingDialog_Title));
            progressDialog.setMessage(getString(R.string.LoadingDialog_Content1));
            progressDialog.show();
        }

        @Override
        protected List<NewsObject> doInBackground(Void... voids) {
            DBHelper db = new DBHelper(context);
            if(isRefresh){
                return loadNews(db);
            }else{
                return db.getNews();
            }
        }

        @Override
        protected void onPostExecute(List<NewsObject> list) {
            super.onPostExecute(list);
            progressDialog.dismiss();
            if(list!=null){
                showNews(context, list);
            }else{
                ReturnMessage.showAlertDialog(context, "Unable to load", "Unable to load news, please try again later.");
            }
        }

        private List<NewsObject> loadNews(DBHelper db) {
            List<NewsObject> newsObjectList = new ArrayList<NewsObject>();
            NodeList newsNodeList = getContentFromXML(getString(R.string.NewsUrl), "post");
            if(newsNodeList != null){
                db.dropNewsTable();
                newsObjectList = convertNewsNodeToObjectList(newsNodeList, db);
            }
            return newsObjectList;
        }

        private NodeList getContentFromXML(String urlString, String tagName) {
            NodeList nodeList = null;
            try {
                URL url = new URL(urlString);
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                Document doc = db.parse(new InputSource(url.openStream()));
                doc.getDocumentElement().normalize();
                nodeList = doc.getElementsByTagName(tagName);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            } catch (SAXException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return nodeList;
        }

        private List<NewsObject> convertNewsNodeToObjectList(NodeList nodeList, DBHelper db) {
            Node node = null;
            List<NewsObject> newsList= new ArrayList<NewsObject>();
            NewsObject news;
            int newsListSize = nodeList.getLength();
            for(int i=0; i<newsListSize; i++){
                node = nodeList.item(i);
                news = new NewsObject();
                news.setTitle(((Element) node).getAttribute("title"));
                news.setDate(((Element) node).getAttribute("publish_date"));
                //news.setContent(((Element) node).getAttribute("content"));
                Element fstElmnt = (Element) node;
                NodeList webContent = fstElmnt.getElementsByTagName("content");
                Element webContentElement = (Element) webContent.item(0);
                webContent = webContentElement.getChildNodes();
                try{
                    news.setContent(((Node) webContent.item(0)).getNodeValue());
                    temp = news.getContent();
                    news.setListContent(temp.replaceAll("\\<.*?>","").replace("\n","").replace("\r",""));
                }catch (Exception e){
                    news.setContent("");
                }
                news.setImageUrl(((Element)node).getAttribute("image"));
                newsList.add(news);
                db.setNews(news);
                //db.setSchedule(schedule);
            }
            return newsList;
        }

        private void showNews(Context context, final List<NewsObject> list) {
            ListView newsListView = (ListView)findViewById(R.id.listview);
            NewsAdapter newsAdapter = new NewsAdapter(context,0,0, list);
            newsListView.setAdapter(newsAdapter);
            newsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    /*Intent intent = new Intent(getParent(), NewsDetail1.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    Bundle b = new Bundle();
                    NewsObject newsObject = list.get(i);
                    b.putString("title", newsObject.getTitle());
                    b.putString("date", newsObject.getDate());
                    b.putString("image", newsObject.getImageUrl());
                    b.putString("content", newsObject.getContent());
                    intent.putExtras(b);
                    startActivity(intent); */
                    NewsObject newsObject = list.get(i);
                    showNewsDetail(newsObject);
                }
            });
        }
    }

    private void showNewsDetail(NewsObject newsObject) {
        newsListLayout.setVisibility(View.GONE);
        newsDetailLayout.setVisibility(View.VISIBLE);
        scrollview.setVisibility(View.VISIBLE);
        isNewsDetailShow = true;
        newsDetailContent.clearCache(true);
        newsDetailContent.clearView();
        newsDetailTitle.setText(newsObject.getTitle());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy");
        try {
			Date dd = simpleDateFormat.parse(newsObject.getDate());
			String dd1 = sdf.format(dd);
			newsDetailDate.setText(dd1);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        newsDetailContent.getSettings().setJavaScriptEnabled(true);
        newsDetailContent.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        newsDetailContent.loadDataWithBaseURL(null, newsObject.getContent(), "text/html", "UTF-8", null);
        imageLoader.displayImage(newsObject.getImageUrl(), newsDetailImage);
        newsDetailContent.requestFocus();
    }

    private class RowData {
        protected int mId;
        protected String mTitle;

        RowData(int id, String title) {
            mId = id;
            mTitle = title;
        }

        @Override
        public String toString() {
            return mId + " " + mTitle;
        }
    }

    private class NewsAdapter extends ArrayAdapter<NewsObject>{
        Context context;
        List<NewsObject> list;
        public NewsAdapter(Context context, int resource,
                           int textViewResourceId, List<NewsObject> list) {
            super(context, resource, textViewResourceId, list);
            this.context = context;
            this.list = list;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            NewsHolder holder = null;
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            if(convertView == null){
                convertView = inflater.inflate(R.layout.newslist1, parent, false);
                holder = new NewsHolder();
                holder.headline = (TextView)convertView.findViewById(R.id.tvNewsHeadline);
                holder.content = (TextView)convertView.findViewById(R.id.tvNewsContent);
                holder.image = (ImageView)convertView.findViewById(R.id.ivNewsImage);
                holder.button = (Button)convertView.findViewById(R.id.btReadMore);
                convertView.setTag(holder);
            }
            else
                holder = (NewsHolder)convertView.getTag();
            NewsObject newsObject = list.get(position);
            holder.headline.setText(newsObject.getTitle());
            holder.content.setMaxLines(4);
            holder.content.setText(newsObject.getListContent());
            imageLoader.displayImage(newsObject.getImageUrl(), holder.image);
            holder.button.setVisibility(View.GONE);
            return convertView;
            /*ViewHolder holder = null;
            TextView headline = null;
            TextView content = null;
            ImageView image = null;
            Button button = null;

            NewsObject newsObject = list.get(position);
            if (null == convertView) {
                convertView = mInflater.inflate(R.layout.newslist, null);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            }
            holder = (ViewHolder) convertView.getTag();
            headline = holder.gettitle();
            headline.setText(rowData.mTitle);

            content = holder.getContent();
            content.setText(tvContent[rowData.mId]);

            image = holder.getImage();
            imageLoader.displayImage(subImage[rowData.mId], image);

            button = holder.getButton();
            button.setVisibility(Button.GONE);
            return convertView;*/
        }

        private class NewsHolder {
            private TextView headline;
            private TextView content;
            private ImageView image;
            private Button button;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.option_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_refresh:
                //LoadContent.LoadNews(getContext());
                new LoadNews(getContext(),true).execute();
                break;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        if(isNewsDetailShow){
            newsListLayout.setVisibility(View.VISIBLE);
            newsDetailLayout.setVisibility(View.GONE);
            scrollview.setVisibility(View.GONE);
            isNewsDetailShow = false;
        }
        else{
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setMessage(getString(R.string.ExitDialog_Content)).setCancelable(false);
            builder.setNegativeButton(getString(R.string.ExitDialog_NoButton), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                }
            });
            builder.setPositiveButton(getString(R.string.ExitDialog_YesButton), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    finish();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }
}