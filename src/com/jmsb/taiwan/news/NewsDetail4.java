package com.jmsb.taiwan.news;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.TextView;
import com.jmsb.taiwan.R;



public class NewsDetail4 extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.newsdetail);

		TextView tvHeadline = (TextView) findViewById(R.id.newsdetailheadline);
		//ImageView ivImage = (ImageView) findViewById(R.id.newsdetailimage);
		TextView tvDate = (TextView) findViewById(R.id.newsdetailpublishdate);
		WebView wvContent = (WebView) findViewById(R.id.newsdetailwebview);
		String bodyStr = "<p>Tottenham star Gareth Bale is reportedly Real Madrid�s top transfer target for next summer, according to Spanish newspaper AS.</p>" +
				"<p>The Spurs winger has been linked with a move to La Liga, and admittedearlier this year that he could see himself playing abroad one day.</p>" +
				"<p>Bale signed a lucrative contract extension in the summer that runs until 2016, but Spanish football expert Guillem Balague understands the Welshman is a transfer priority for the nine-time European champions.</p>" +
				"<p>Real Madrid raided White Hart Lane to sign Luka Modric after a protracted transfer saga, and the La Liga champions could return at the end of this season to bid for Bale.</p>" +
				"<p>Balague speculates Madrid could have to pay as much as �50m for the Tottenham winger, but El Clasico rivals Barcelona could also be tempted to move for the winger.</p>" +
				"<p>The 23-year-old would form one of the most formidable attacking lineups in world football. With Bale on the left, Cristiano Ronaldo on the right, Karim Benzema or Gonzalo Higuain through the middle and Mesut Ozil or Luka Modric in behind, Mourinho would have one of the most expensive forward lines in history.</p>" +
				"<p>Bale revealed in an earlier interview with AS that he grew up admiring the likes of Zinedine Zidane, Luis Figo and Ronaldo, and a chance to follow in the footsteps of his heroes could be difficult to turn down.</p>" +
				"<p>The Tottenham midfielder joined the north London club from Southampton, becoming one of the most expensive teenage transfers in British football history in the process.</p>" +
				"<p>Any new bid for the 23-year-old would likely now make him one of the most expensive players of all-time.</p>";
		
		tvHeadline.setText("�50m Tottenham star is Real Madrid�s top summer target");
		//ivImage.setImageDrawable(getResources().getDrawable(R.drawable.rmnewimage4));
		tvDate.setText("30-October-2012");
		wvContent.getSettings().setJavaScriptEnabled(true);
		wvContent.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
		wvContent
				.loadDataWithBaseURL(null, bodyStr, "text/html", "UTF-8", null);

	}
}