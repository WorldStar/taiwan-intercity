package com.jmsb.taiwan.news;

/**
 * Created with IntelliJ IDEA.
 * User: jmsb
 * Date: 8/14/13
 * Time: 11:52 AM
 * To change this template use File | Settings | File Templates.
 */
public class NewsObject {
    String title;
    String date;
    String content;
    String imageUrl;
    String listContent;

    public String getListContent() {
        return listContent;
    }

    public void setListContent(String listContent) {
        this.listContent = listContent;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
