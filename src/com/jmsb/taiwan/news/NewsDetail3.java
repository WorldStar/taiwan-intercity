package com.jmsb.taiwan.news;

import com.jmsb.taiwan.R;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;



public class NewsDetail3 extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.newsdetail);

		TextView tvHeadline = (TextView) findViewById(R.id.newsdetailheadline);
		//ImageView ivImage = (ImageView) findViewById(R.id.newsdetailimage);
		TextView tvDate = (TextView) findViewById(R.id.newsdetailpublishdate);
		WebView wvContent = (WebView) findViewById(R.id.newsdetailwebview);
		String bodyStr = "<p>Manchester United transfer target Mesut Ozil insists he�s happy with life in Madrid after speculation mounted he fallen out with Real Madrid manager Jose Mourinho.</p>" +
				"<p>The Germany international was thought to be a target for Manchester United and Liverpool, but Ozil has reaffirmed his commitment to the La Liga champions.</p>" +
				"<p>Ozil was one of Madrid�s best players last season, as Jose Mourinho�s side romped to their first league title since 2008. The Germany star was instrumental and led the league in assists, but suffered a dip in form at the beginning of this campaign, amid pressure on his place from new signing Luka Modric.</p>" +
				"<p>And according to Goal.com, reports suggested the former-Werder Bremen star had fallen foul of Mourinho, and was benched as a punishment.</p>" +
				"<p>But Ozil returned to the starting XI for 2-2 draw against Barcelona at the Nou Camp last weekend, and laid on a magnificent assist for Ronaldo�s equaliser in the second half.</p>" +
				"<p>And Ozil has now made it plain that his relationship with Mourinho is strong, and that he�s enjoying life in the Spanish capital.</p>" +
				"<p>�Mourinho knows what I can do and he has faith in me,� Ozil told Kicker magazine. �I�m grateful for the confidence he has shown in me.</p>" +
				"<p>�Walking around the streets of Madrid is unbelievable. The fans are incredible. Naturally they make me feel proud and happy.</p>" +
				"<p>�The atmosphere when I play in the Santiago Bernabeu is special. It�s amazing.�</p>" +
				"<p>Manchester United signed Shinji Kagawa for �17m from Borussia Dortmund in the summer, and the Japan international would likely occupy the same space as the Madrid star if they played in the same lineup.</p>" +
				"<p>But Manchester United are still thought to be keen to sign a new central midfielder. With Paul Scholes and Ryan Giggs approaching retirement, Sir Alex Ferguson could use the January transfer window to strengthen his squad.</p>" +
				"<p>The most recent midfielder to be linked with Manchester United is PSV star and Holland captain Kevin Strootman, although the Dutch club insist he will not be sold in January.</p>";
	
		tvHeadline
				.setText("United�s German transfer target happy in La Liga");
		//ivImage.setImageDrawable(getResources().getDrawable(R.drawable.rmnewimage3));
		tvDate.setText("30-October-2012");
		wvContent.getSettings().setJavaScriptEnabled(true);
		wvContent.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
		wvContent
				.loadDataWithBaseURL(null, bodyStr, "text/html", "UTF-8", null);

	}
}